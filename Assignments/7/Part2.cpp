#include <iostream>
#include <string>
#include <iomanip>
using namespace std;

class country
{
public:
	country();
	country (string n, long p, double s);

	void set_name(string in_n);
	void set_pop(long in_p);
	void set_area(double in_a);
	void raise_price(double in_inc);

	string get_name() const;
	long get_pop() const;
	int get_area() const;
	double get_density() const;

	bool is_better_than(country b) const;

private:
	string country_name;
	long country_pop;
	int country_area;

};

country::country()
{
	country_name = "NONE YET";
	country_pop = 0.01;
	country_area= 0;
}
country::country(string in_name, long in_pop, double in_area)
{
	country_name = in_name;
	country_pop = in_pop;
	country_area = in_area;

}

void country::set_name(string in_name)
{
	country_name = in_name ;
}
void country::set_pop(long in_pop)
{
	country_pop = in_pop ;
}
void country::set_area(double in_area)
{
	country_area = in_area ;
}
string country::get_name() const
{
	return (country_name);
}
long country::get_pop() const
{
	return (country_pop);
}
int country::get_area() const
{
	return (country_area);
}
double country::get_density() const
{
	double value = 0.0;
	if (country_pop > 0)
		value = country_pop / country_area;
	if (country_pop <= 0)
		value = 9999999.9;
	return value;

	bool country::is_better_than(country b) const
	{
		bool iam_better = true;
		int situation = 0;
		double in_b_pop = 0.0;
		in_b_pop = b.get_pop();
		if ( (in_b_pop == 0) && (country_pop > 0) )
			situation = 1;
		if ((country_pop == 0) && (in_b_pop > 0))
			situation = 2;
		if ((country_pop == 0) && (in_b_pop == 0))
			situation = 3;
		if ( (country_pop > 0) && (in_b_pop > 0))
			situation = 4;

		switch (situation)
		{
		case 1: iam_better = false;
			break;
		case 2: iam_better = true;
			break;
		case 3: iam_better = true;
			break;

			if ( get_density() > b.get_density() )
				iam_better = true;
			else iam_better = false;
			break;
		default:
			iam_better = false;
			break;
		}
		return iam_better;
	}

}

int main ()
{

	string c_name = "";
	long c_pop = 0.0;
	int c_area = 0.0;
	int winner_area=0.0;
	long winner_pop=0.0;
	int winner_density=0.0;
	bool more=true;
	country top_country;

	//to hold info of the top country , the incumbent

	cout << "Top countries are: " << endl;
	cout << "Country Name: " << top_country.get_name() << endl;
	cout << fixed << setprecision(2);
	cout << "Country Population: " << top_country.get_pop() << endl;
	cout << "Country Area: " << top_country.get_area() << endl;
	cout << "Population Density: " << top_country.get_density() << endl;
	cout << " " << endl;

	cout << "Please enter the first country:";
	cin >> c_name;
	cin.ignore(256, '\n');
	cout << "Please enter the population of this country in a whole figure:";
	cin >> c_pop;
	cout << "Please enter the area of this country in square kilometers: ";
	cin >> c_area;
	cout << "  "<< endl;

	country c1(c_name, c_pop, c_area);

	cout << "Please enter the second country:";
	cin >> c_name;
	cin.ignore(256, '\n');
	cout << "Please enter the population of this country in a whole figure:";
	cin >> c_pop;
	cout << "Please enter the area of this country in square kilometers: ";
	cin >> c_area;
	cout << "  "<< endl;
	country c2(c_name, c_pop, c_area);
	c2.set_name(c_name);
	c2.set_pop(c_pop);
	c2.set_area(c_area);
	cout<< "  "<< endl;

	cout << "Please enter the third country:";
	cin >> c_name;
	cin.ignore(256, '\n');
	cout << "Please enter the population of this country in a whole figure:";
	cin >> c_pop;
	cout << "Please enter the area of this country in square kilometers: ";
	cin >> c_area;
	cout << "  "<< endl;
	country c3(c_name, c_pop, c_area);
	c3.set_name(c_name);
	c3.set_pop(c_pop);
	c3.set_area(c_area);
	cout<< "  "<< endl;

	cout << "Please enter the fourth country:";
	cin >> c_name;
	cin.ignore(256, '\n');
	cout << "Please enter the population of this country in a whole figure:";
	cin >> c_pop;
	cout << "Please enter the area of this country in square kilometers: ";
	cin >> c_area;
	cout << "  "<< endl;
	country c4(c_name, c_pop, c_area);
	c4.set_name(c_name);
	c4.set_pop(c_pop);
	c4.set_area(c_area);
	cout<< "  "<< endl;

	cout << "Please enter the fifth country:";
	cin >> c_name;
	cin.ignore(256, '\n');
	cout << "Please enter the population of this country in a whole figure:";
	cin >> c_pop;
	cout << "Please enter the area of this country in square kilometers: ";
	cin >> c_area;
	cout << "  "<< endl;
	country c5(c_name, c_pop, c_area);
	c5.set_name(c_name);
	c5.set_pop(c_pop);
	c5.set_area(c_area);
	cout<< "  "<< endl;


	cout << "Final Winner: " << endl;
	cout << "Country with the largest population:" << top_country.get_name()
		<<top_country.get_pop()<<endl;
	cout << "Country with the largest area:" << top_country.get_name()
		<<top_country.get_area<< endl;
	cout << "Country with the highest population density: "<< top_country.get_name()
		<<top_country.get_density()<< endl;
	cout << " " << endl;
	cout << "Bye!" << endl;
	return 0;

}