#include <iostream>
using namespace std;

int main() {
    int number;//setting integer number
    cout << "Please enter a year: ";//prompt user
    cin >> number;//receives number 
    if (((number <= 1582)) || (( number % 4 == 0 && ( number % 100 != 0 || number % 400 == 0 ) )))//the if statements to show this is a leap year 
        {cout << number << " is a leap year." << endl;}//if output 
    else//if the input does not follow the if, the else will be applied 
        {cout << number << " isn't a leap year." << endl;}
	return 0;
}