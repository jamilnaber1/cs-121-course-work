#include <iostream>
#include <string>
using namespace std;

int main() {
    string romnum; //initialize string
    int input; //initialize user input
    cout << "Enter the whole number less than 4000 you wish to convert: "; //prompt user
    cin >> input; //get input
    int m, d, c, l, x, v, i; //initialize more placeholder values
    
if (((input >=4000)) || ((input <= 0))) //restricts user to integer range of 1-3999 
        { cout << "\nInvalid Input!"; } //tells user it is wrong input
else
{
        
    if ((input >= 1000)) //this begins adding M's
    {   
        m = input / 1000; //gives the input in thousands
        romnum += string(m, 'M'); //adds 'M', m-times to string romnum, eliminating need for loop
        input = input%1000;  //get the remainder after dividing by 1000
    }
    if ((input >= 900)) //special case CM
    {
        romnum += "CM"; //adds CM if 9 is present in hundreds place
        input = input%900; //get remainder after dividing 900
    }
        else if ((input >= 500)) //condition to print the D's
        {
            d = input / 500; //gives the remaining input in 500s
            romnum += string(d, 'D'); //adds 'D', d-times to string romnum, eliminating need for loop
            input = input%500; //get the remainder after dividing by 500
        } //    THE REST IS THE SAME PROCESS
    if ((input >= 400)) //special case CD
    {
        romnum += "CD";  //adds CD if 4 is present in the hundreds place
        input = input%400; //get remainder after dividing by 400
    }
        else if ((input >= 100)) //C's
        {
            c = input / 100; //gives the remaining input in 100s
            romnum += string(c, 'C'); //adds 'C', c-times to string romnum, eliminating need for loop
            input = input%100; //get remainder after dividing by 100
        }
    if ((input >= 90)) //special case XC
    {
        romnum += "XC"; //adds XC if 9 is present in the tens place
        input = input%90; //get remainder after dividing by 90
    }
        else if ((input >= 50)) //L's
        {
            l = input / 50; //gives the remaining input in 50s
            romnum += string(l, 'L'); //adds 'L', l-times to string romnum, eliminating need for loop
            input = input%50; //get remainder after dividing by 50
        }
    if ((input >= 40)) //special XL
    {
        romnum += "XL"; //adds XL if 4 is present in the tens place
        input = input%40; //get remainder after dividing by 40
    }
        else if ((input >=10)) //X's
        {
            x = input / 10; //gives the remaining input in 10s
            romnum += string(x, 'X'); //adds 'X', x-times to string romnum, eliminating need for loop
            input = input%10; //get remainder after dividing by 10
        }
    if ((input >= 9)) //special case IX
    {
        romnum += "IX"; //adds IX if 9 is present in the ones place
        input = input%9; //get remainder after dividing by 9
    }
        else if ((input >= 5)) //V's
        {
            v = input / 5; //gives the remaining input in 5s
            romnum += string(v, 'V'); //adds 'V', v-times to string romnum, eliminating need for loop
            input = input%5; //get remainder after dividing by 5
        }
    if ((input >= 4)) //special case IV
    {
    romnum += "IV"; //adds IV if 4 is present in the ones place
    input = input%4; //get remainder after dividing by 4
    }
    else if ((input >= 1)) //I's
    {
        i = input; //gives the remaining input 
        romnum += string(i, 'I'); //adds 'I', i-times to string romnum, eliminating need for loop
    }

}
cout << romnum; //prints final output
	return 0;
}
