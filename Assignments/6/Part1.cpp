#include <iostream>
using namespace std;

int toDigit(char&);
int ReadDials(char&, char&, char&, char&, char&, char&, char&, char&);
void AcknowledgeCall(char, char, char, char, char, char, char, char);

int main()
{
	char d1=' ';
	char d2=' ';
	char d3=' ';
	char d4=' ';
	char d5=' ';
	char d6=' ';
	char d7=' ';
	char d8=' ';
	int cvalue = 1;

	while (cvalue !=-5)
	{
		cvalue = ReadDials(d1, d2, d3, d4, d5, d6, d7, d8);

		switch (cvalue)
		{
			case -5:	break;
			case -1:	cout << "ERROR - An invalid character was entered." << endl;
						break;
			case -2:	cout << "ERROR - Phone number cannot begin with 0" << endl;
						break;
			case -3:	cout << "ERROR - Phone number cannot begin with 555" << endl;
						break;
			case -4:	cout << "ERROR - Hyphen is not in the correct position" << endl;
						break;
			case 0:		AcknowledgeCall(d1, d2, d3, d4, d5, d6, d7, d8);
					break;
			default:	break;
		}
	}

	return 0;
}	

int ReadDials(char& rd1, char& rd2, char& rd3, char& rd4, char& rd5, char& rd6, char& rd7, char& rd8)
{
	cout << "Enter a phone number (Q to quit): ";
	cin >> rd1;
	if ((rd1=='Q') || (rd1=='q'))
		return -5;
	cin >> rd2 >> rd3 >> rd4 >> rd5 >> rd6 >> rd7 >> rd8;

	if (rd1=='0')
		return -2;
	if ((rd1=='5') && (rd2=='5') && (rd2=='5') )
		return -3;
	if (rd4!='-')
		return -4;
	if ((toDigit(rd1) == 0) && (toDigit(rd2) == 0) && (toDigit(rd3) == 0) && (toDigit(rd5) == 0) && (toDigit(rd6) == 0) && (toDigit(rd7) == 0) && (toDigit(rd8) == 0))
		return 0;
	else
		return -1;
}

int toDigit(char& td)
{
		
	switch (td)
	{
		case 'a': 
		case 'b':
		case 'c':
		case 'A':
		case 'B':
		case 'C': td = '2'; 
				return 0;
				break;
		case 'd':
		case 'e':
		case 'f':
		case 'D':
		case 'E':
		case 'F': 	td = '3'; 
				return 0;
				break;
		case 'g': 
		case 'h':
		case 'i':
		case 'G':
		case 'H':
		case 'I': 	td = '4'; 
				return 0; 
				break;
		case 'j': 
		case 'k':
		case 'l':
		case 'J':
		case 'K':
		case 'L': 	td = '5'; 
				return 0; 
				break;
		case 'm': 
		case 'n':
		case 'o':
		case 'M':
		case 'N':
		case 'O': 	td = '6'; 
				return 0; 
				break;
		case 'p': 
		case 'q':
		case 'r':
		case 's':
		case 'P':
		case 'Q':
		case 'R': 
		case 'S': 	td = '7'; 
				return 0; 
				break;
		case 't': 
		case 'u':
		case 'v':
		case 'T':
		case 'U':
		case 'V': 	td = '8'; 
				return 0; 
				break;
		case 'w': 
		case 'x': 
		case 'y':
		case 'z':
		case 'W':
		case 'X':
		case 'Y':
		case 'Z': 	td = '9'; 
				return 0; 
				break;
		case '0': 
		case '1':
		case '2': 
		case '3': 
		case '4': 
		case '5': 
		case '6': 
		case '7': 
		case '8': 
		case '9': 	return 0; 
				break;
		default: 	return -1; 
				break;
	}
}

void AcknowledgeCall(char a1, char a2, char a3, char a4, char a5, char a6, char a7, char a8)
{
	cout << "Phone Number Dialed: " << a1 << a2 << a3 << a4 << a5 << a6 << a7 << a8 << endl;
}