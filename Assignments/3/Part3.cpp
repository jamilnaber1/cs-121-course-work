#include<iostream>
#include<string>
#include<cmath>

using namespace std;

int main()//all of the above is standard c++ being code
{
    string lettergrade = "";//made a string variable so we can input letters
    cout << "Enter a letter grade: " << endl;//prompts the user to enter a letter grade 
    cin >> lettergrade;//receives letter grade from user
    //set up a selection structure
    
    if (lettergrade == "A+")//make an if then statement for an string input that equal to the letter grade
        cout << "The numeric value is: " << 4.0 << endl;//prompts the users the numeric equal value of their letter grade
    else if (lettergrade == "A")//else if statement is created so the letter can be pass down the line to find a number
       cout << "The numeric value is: " << 4.0 << endl;
    else if (lettergrade == "A-")
       cout << "The numeric value is: " << 3.7 << endl;
    else if (lettergrade == "B+")
       cout << "The numeric value is: " << 3.3 << endl;
    else if (lettergrade == "B")
       cout << "The numeric value is: " << 3.0 << endl;
    else if (lettergrade == "B-")
       cout << "The numeric value is: " << 2.7 << endl;
    else if (lettergrade == "C+")
       cout << "The numeric value is: " << 2.3 << endl;
    else if (lettergrade == "C")
       cout << "The numeric value is: " << 2.0 << endl;
    else if (lettergrade == "C-")
       cout << "The numeric value is: " << 1.7 << endl;
    else if (lettergrade == "D+")
       cout << "The numeric value is: " << 1.3 << endl;
    else if (lettergrade == "D")
       cout << "The numeric value is: " << 1.0 << endl;
    else if (lettergrade == "D-")
       cout << "The numeric value is: " << 0.7 << endl;
    else if (lettergrade == "F+")
       cout << "The numeric value is: " << 0.3 << endl;
    else if (lettergrade == "F")
       cout << "The numeric value is: " << 0.0 << endl;
    else if (lettergrade == "F-")
       cout << "The numeric value is: " << 0.0 << endl;
    else //the else statment applies to a letter not found in the selection
       cout << "This is not a Letter grade " << endl;
    return 0;
    
}