#include <iostream>
using namespace std;

int main()
{
	cout << "Enter a purchase amount to find out your shipping charges." << endl; //asks user for input
	double shipping_charges; //declares variable
	cin >> shipping_charges; //user input 
	
	if ((shipping_charges <= 250) && (shipping_charges > 0)) //$5 range condition
		{
		cout << "The shipping charge on a purchase of $" << shipping_charges << " is $5.00." << endl; //body if true
		}
	else if ((shipping_charges <= 500) && (shipping_charges >= 250.01)) //$8 range condition
		{
		cout << "The shipping charge on a purchase of $" << shipping_charges << " is $8.00." << endl; //body if true
		}
	else if ((shipping_charges <= 1000) && (shipping_charges >= 500.01)) //$10 range condition
		{
		cout << "The shipping charge on a purchase of $" << shipping_charges << " is $10.00." << endl; //body if true
		}
	else if ((shipping_charges <= 5000) && (shipping_charges >= 1000.01)) //$15 range condtion
		{
		cout << "The shipping charge on a purchase of $" << shipping_charges << " is $15.00." << endl; //body if true
		}
	else if ((shipping_charges > 5000)) //$20 range condition
		{
		cout << "The shipping charge on a purchase of $" << shipping_charges << " is $20.00." << endl; //body if true
		}
	else 
		cout << "Wrong purchase amount!" << endl; //body if negative or any other input is given
	return 0;
}