#include <iostream>
#include <string>
#include <cmath>
using namespace std;

int main() 
{
    /*//got to loop this process 3 times to get 3 divers/cities !!Commented out for now
    int name; //initializations of name and city
    int city;
    cout << "Enter the diver's name: ";
    cin >> name;
    cout << "\nEnter the city: ";
    cin >> city;*/
    char again;
    double g = 0;
    double totalAvgScore = 0;
    string event = "";
    cout <<"Event: ";
    getline(cin.ignore(0,'n'), event);
    
    
    do
    {
    
    int counter = 1;
    double score = 0;
    double largest = 0;
    double smallest = 10;
    double total = 0;
    
    string dname = "";
    string cname = "";
    cout <<"\nEnter the diver's name: ";
    getline(cin.ignore(0,'n'), dname);
    cout <<"Enter the diver's city: ";
    getline(cin.ignore(0,'n'), cname);
    
    for (int i = 1; i <= 5; i++)
    {
        while (counter <= 5)
        {
            cout << "Enter the score given by judge #" << counter << ": ";
            cin >> score; //user input
            while ((score > 10) || (score < 0))
            { 
                cout << "Invalid score - Please reenter (Valid Range: 0 - 10) "<< endl;
                cout << "Enter the score given by judge #" << counter << ": ";
                cin >> score; 
            }
            
            
            if (largest < score)
            {
                largest = score; 
            }
            if (smallest > score) 
            {
                smallest = score;
            }
            
            total += score;
            counter++;
        }
    }
    
    double dOd;
    cout << "What was the degree of difficulty? ";
    cin >> dOd;
        while ((dOd > 1.67) || (dOd < 1))
        {
            cout << "Invalid degree of difficulty - Please reenter (Valid Range: 1 - 1.67)";
            cout << "\nWhat was the degree of difficulty? ";
            cin >> dOd; 
        }
    
    //computations
    double newTotal = total - largest - smallest;
    double overallScore = (newTotal / 3) * dOd;
    cout << "Driver: " << dname << ", City: "<< cname<< endl;
    cout << "Overall score was " << overallScore << endl;
    
    totalAvgScore += overallScore;
    
    cout << "\nDo you want to process another diver (Y/N)? ";
    cin >> again;
    g++;
    
    } while ((again == 'Y')||(again == 'y'));
    
    cout <<"\n\n\t EVENT SUMMARY"<< endl;
    cout << "Number of divers participating: "<< g <<endl;
    cout <<"Average score of all divers: " << totalAvgScore / g <<endl;
    cout << "Press any key to continue . . .";
	return 0;
}
