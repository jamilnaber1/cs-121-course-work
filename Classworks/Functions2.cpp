#include <iostream>
#include <string>
#include <cmath>

using namespace std;

void Header(string);

int main ()
{
    string title = "\nThis value is passed to the function\n";
    Header(title);
    return 0;
}

void Header(string T)
{
    cout << T << endl;
}