#include <iostream>
#include <string>

using namespace std;

class Address 
{
    private:
       string house_number;
       string street;
       string apartment_number;
       string city;
       string state;
       string postal_code;
       
    public:
       Address();
       Address(string, string, string, string, string, string);
       Address(string, string, string, string, string);
       void prstring() const;
       bool comes_before(Address a) const;
       bool operator<(Address);
};

Address::Address()
{
    house_number = "";
    street = "";
    apartment_number = "";
    city = "";
    state = "";
    postal_code = "";
}
Address::Address(string h, string st, string appt, string c, string s, string pc)
{
    house_number = h;
    street = st;
    apartment_number = appt;
    city = c;
    state = s;
    postal_code = pc;
}

Address::Address(string h, string st, string c, string s, string pc)
{
    house_number = h;
    street = st;
    city = c;
    state = s;
    postal_code = pc; 
}

void Address::prstring() const
{
    cout << house_number << " " << street;
    if (apartment_number != "")
        cout << ", #" << apartment_number;
    cout << "\n";
    cout << city << ", " << state << ", "<<postal_code;
    cout << "\n";
}

bool Address::comes_before(Address other) const
{
    return postal_code < other.postal_code;
}

bool Address::operator<(Address S)
{
    if (postal_code < S.postal_code)
       return true;
    else
       return false;
}

int main()
{
    Address my_place("148", "First Ave", "San Diego", "CA", "10396");
    Address your_place("553", "Second St", "448", "San Diego", "CA", "10394");
    
    cout << "Comparing address\n";
    my_place.prstring();
    cout << "\nwith address\n";
    your_place.prstring();
    if (my_place.comes_before(your_place))
       cout << "\nThe first address comes before the seacond\n";
    else
       cout << "\nThe first address comes after the second\n";
    if (my_place < your_place)
       cout << "\nThe first address comes before the seacond\n";
    else
       cout << "\nThe first address comes after the second\n";
    return 0;
}