#ifndef ADD_H
#define ADD_H

using namespace std;

class Address 
{
    private:
       string house_number;
       string street;
       string apartment_number;
       string city;
       string state;
       string postal_code;
       
    public:
       Address();
       Address(string, string, string, string, string, string);
       Address(string, string, string, string, string);
       void prstring() const;
       bool comes_before(Address a) const;
       bool operator<(Address);
};

#endif