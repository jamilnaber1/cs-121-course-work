/*"This program introduces datatypes/variables/declaration and initialization
along with size of the dat type.*/
#include <iostream>
#include <string>
using namespace std;

int main()
{
  string firstName; //declaration of a variable, has garbage value right now
  firstName = "John"; //initializtion of the variable 
  string lastName = "Smith"; //declaration and initialization at the same time
  int ID = 555;
  float gpa = 3.54;
  double hairsize = 0.0000000036;
  char grade = 'A';
  bool tuition = true;
  cout <<"STUDENT INFROMATION\n" << endl;
  cout <<"First Name:\t" << firstName << endl;
  cout <<"Last Name:\t" << lastName << endl;
  cout <<"ID:\t\t" << ID << endl;
  cout <<"GPA:\t\t" << gpa << endl;
  cout <<"Hair Size:\t" <<hairsize << endl;
  cout <<"Grade:\t\t" <<grade<< endl;
  cout <<"Tuition Paid:\t" << tuition << endl;
  
  //writing code to find how much space is needed by each datatype
  //sizeof() is a fuction that returns the size of the datatypes given in
  cout << "Integer size is: " << sizeof(int) << " bytes" << endl;
  cout << "Short size is:" <<sizeof(short) << " bytes" << endl;
  cout << "Long size is:" <<sizeof(long) << " bytes" << endl;
  cout << "float size id: " << sizeof(float) << " bytes" << endl;
  cout << "Double size is: " << sizeof(double) << " bytes" << endl;
  cout << "Character size is: " << sizeof(char) << " bytes" << endl;
  cout <<"Bool size is: " <<sizeof(bool) <<" bytes" <<endl;
  return 0;
}
