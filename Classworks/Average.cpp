#include<iostream>
#include<string>
#include<cstdlib>
#include<ctime>
using namespace std;

int main()
{
  int smallest = INT_MAX;
  int largest = INT_MIN;
  
  int howmany;
  cout << "How many numbers would you like to average?";
  cin >> howmany;
  
  int i, num, total = 0;
  for(i = 0; i<howmany; i++)
  {
     cout << "Please enter the number: ";
     cin >> num;
     total = total + num;
     if (num < smallest)
        smallest = num;
     if (num > largest)
        largest = num;
  }
  cout << "\n\nAverage of the numbers is: "<< static_cast <float>(total)/static_cast <float>(i) << endl;
  cout << "Largest of the numbers is: " << largest << endl;
  cout << "Smaller of the numbers is: " << smallest << endl; 
  return 0;
  
 }
