#include <iostream>
#include <string>
#include <cmath>

using namespace std;

int Product(int, int);

int main()
{
    int n1 , n2;
    cout << "Please enter two integers: ";
    cin >> n1 >> n2;
    cout << "The product is " << Product(n1,n2) << endl;
    return 0;
}

int Product(int N1, int N2)
{
    return (N1 * N2);
}