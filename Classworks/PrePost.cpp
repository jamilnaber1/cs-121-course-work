#include<iostream>
using namespace std;
int main()
{
   int x;
   x = 25;
   cout << "\nThe value of x is: "<< x << endl;
   cout << "the address of x is: "<< &x << endl;
   cout << "After post increment x is: " << x++ << endl;
   cout << "\nThe value of x is: " << x << endl;
   
   cout <<"After pre increment: " << ++x << endl;
   cout <<"After pre decrement: " << --x << endl;
   cout <<"After post decrement: " << x-- << endl;
   cout <<"\nThe value of x is: " << x << endl;
   
   cout << endl;
   return 0;
 }
