#include <iostream>
#include <string>

using namespace std;
struct label
{
    string fname;
    string lname;
    int zip;
};

void print(label);
void Getinput(label&);

int main()
{
    label mailing, person;
    Getinput(person);
    print(person);
    
    mailing.fname="John";
    mailing.lname="Smith";
    mailing.zip=60101;
    print(mailing);
    return 0;
}

void print(label p)
{
    cout << p.fname <<"\t" << p.lname << endl;
    cout << p.zip << endl << endl;
}

void Getinput(label& p)
{
    cout << "Enter the person first name , last name and zip: ";
    getline(cin, p.fname);
    getline(cin, p.lname);
    cin >> p.zip;
    cin.ignore();
}