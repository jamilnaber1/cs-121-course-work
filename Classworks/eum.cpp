#include <iostream>
using namespace std;
enum size { small = 10, medium = 20, large, extra, special};

int main()
{
    size mysize, yoursize, hersize;
    mysize = medium;
    yoursize = static_cast <size> (mysize + 2);
    cout << mysize << "\t" << yoursize << endl;
    hersize = static_cast <size> (small + large);
    cout << hersize << endl;
    return 0;
}