//this exercise is to work with mixed datatypes for compatible types
//save this file as staticCasting
#include<iostream>
using namespace std;
int main()
{
  int x = 65;
  float y;
  y = x / 2;//assigns int value to flat variable 
  y = static_cast <float> (x / 2.0);//assigns float value
  x = y;//assign floatt value to int varaible
  x = static_cast <int> (y);
  cout << x << "\t" << y << endl;
  
  x = 65;//resign 65 to x 
  char w;//declare a character variable
  w = static_cast <char> (x);//convert 65 to character
  cout << x << "\t" << w << endl;//print to see output
  cout << (w + 25) << endl;//print numeric value, doing math with char type
  cout << static_cast <char> (w + 25) << endl;//casting to print character for 90
  
  bool a, b = true;//declare boolean variables
  cout << b << endl;//print b---> outputs as 1
  a = static_cast <bool> (b - 1);//b is 1, subtract 1 from 1 which is 0 (false)
  cout << a << endl;//print false, outputs as 0
 
  return 0;
}
