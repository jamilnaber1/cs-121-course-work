#include <iostream>
#include <string>
#include <cmath>

using namespace std;

void Perfect();
void EvenOdd();
void LetterGrade();
void Condn();
int MENU ();

int main()
{
    int menu = MENU();
    
    switch (menu)
    {
        case 1:
        {
            Perfect();
            break;
        }
        case 2:
        {
            EvenOdd();
            break;
        }
        case 3:
        {
            LetterGrade();
            break;
        }
        case 4:
        {
            Condn();
            break;
        }
        case 9:
        {
            cout << endl;
            return 0;
        }
    }
    return 0;
    
}

void Perfect()
{
    int score;
    cout << "Please enter a value for score: ";
    cin >> score;
    cout << "Here is the value " << score <<endl;
    
    if (score == 100)
    {
        cout <<"\nPrefect score!\n";
        cout <<"\nBravo! Great!\n";
    }
}
void EvenOdd()
{
    int score; 
    cout <<"Please enter a value for score: ";
    cin >> score;
    cout << "Here is the value " << score << endl;
    if ((score % 2) == 0)
        cout << "\nScore is even!\n\n";
    else 
        cout <<"Score is odd! \n\n";
}
void LetterGrade()
{
    int score;
    cout <<"Please enter a value for score: ";
    cin >> score;
    cout << "Here is the value " << score << endl;
    
    if (score >= 90)
       cout << "\nGrade is A\n\n";
    else if (score >= 80)
       cout << "\nGrade is B\n\n";
    else if (score >= 70)
       cout << "\nGrade is C\n\n";
    else if (score >= 60)
       cout << "\nGrade is D\n\n";
    else
       cout <<"\nGrade is F\n\n";
}

void Condn()
{
    int score;
    cout <<"Please enter a value for score: ";
    cin >> score;
    cout << "Here is the value " << score << endl;
    ((score % 2) == 0) ? cout <<"\nScore is Even\n\n" : cout << "\nScore is odd\n\n";
}
int MENU ()
{
    cout <<"\nPlease choose from the following menu options:\n";
    cout <<"1.  Prefect score check \n";
    cout <<"2.  Even/Odd check \n";
    cout <<"3.  Grade based on score check\n";
    cout <<"4.  Conditional Operator used for Even/Odd check\n";
    cout <<"9.  Quit\n";
    int menu = 0;
    cin >> menu;
    return menu;
}