#include <iostream>
#include <string>
#include <cmath>

using namespace std;

int Product(int , int);

int main()
{
    int Num1, Num2;
    cout << "Please enter the first number: ";
    cin >> Num1;
    cout << "Please enter the seacond number: ";
    cin >> Num2;
    cout << "Product of " << Num1 << " and " << Num2 <<" is " << Product(Num1, Num2) << endl;
    cout << Num1 << "\t" << Num2 << endl;
    return 0;
}

int Product(int N1, int N2)
{
    N1++;
    N2++;
    return (N1 * N2);
}