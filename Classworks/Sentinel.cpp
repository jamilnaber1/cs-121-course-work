#include<iostream>
#include<cmath>
#include<string>

using namespace std;

int main()
{
    int num = 1;
    char again;
    cout << "Please enter Y to continue this program ";
    cin >> again;
    while ((again == 'Y')||(again == 'y'))
    {
        cout << "The value of the number is " << num << endl;
        num++;
        cout << "Do you want to print another number? ";
        cin >> again;
    }
    cout << endl;

return 0;
}