#include <iostream>
using namespace std;

int main()
{
/*int x = 25;
int y = 50;
int temp;
temp = y;
y = x;
x = temp;

cout << "x and y swapped is " << x << " " << y << " with direct swap." << endl;*/

int a = 10,b = 15,*c, *d;
cout << "Here is a and b before the swap " << a << " " << b << endl;
c = &b;
cout << a << " " << c << " " << *c << endl;
d = &a;
cout << b << " " << d << " " << *d << endl;
cout << *c << " " << *d << endl;
*c = a;
*d = b;
cout << "Here is a and b after the swap " << a << " " << b << endl;

return 0;
}
