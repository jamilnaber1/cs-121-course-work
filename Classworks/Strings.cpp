#include<iostream>
#include<string>
using namespace std;

int main()
{
   int StdId = 0;
   string name = "";
   double gpa = 0.0;
   cout << "\n Please enter the values......" << endl;
   cout << "Enter the Id: ";
   cin >> StdId;
   cin.ignore();//this ignores the new line
   cout <<"Enter the full name:";
   //cin >> name;//this works for string with one word or no spaces 
   getline(cin, name);//use this to read a string with spaces
   
   cout << name << endl;
   cout <<"Enter gpa:";
   cin>> gpa;
   
   cout << "\n Here are the values " << endl;
   cout << "Student ID: " << StdId << endl;
   cout << "Student Name: " << name << endl;
   cout << "Length of the student name: " << name.length() << endl;
   cout << "Student GPA:\t " << gpa << endl;
   
   //substr(0,7) first number tells where to start, 2nd says how amny charcter to get
   string fname = name.substr(0,5);
   string lname = name.substr(6);
   cout << "Student First Name: \t\t" << fname << endl;
   cout << "Student Last Name: \t\t" << lname <<endl;
   
   cout <<"using concatenation to get full name:\t " << fname +""+ lname << endl;
   return 0;
   }
