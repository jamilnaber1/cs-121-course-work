#include <iostream>
#include <vector>
using namespace std;
void reverse(vector<int> &v);

int main()
{
    vector<int> v;
    int c;
    cout << "Enter a value (-999 to quit): ";
    cin >> c;
    while(c !=-999)
    {
        v.push_back(c);
        cout << "Enter a value (-999 to quit): ";
        cin >> c;
    }

    reverse(v);
    cout<<"Vector elements are after reversing: "<<endl;

    for(int i=0;i<v.size(); i++)
    {
        cout<<v[i]<<" ";
    }

    return 0;
}

void reverse(vector<int> &v) 
{
    for(int i=0;i<v.size()/2; i++)
    {
        int t = v[i];
        v[i]=v[v.size()-1-i];
        v[v.size()-1-i]=t;
    }
}