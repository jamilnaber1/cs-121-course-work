#include <iostream>
#include <vector>
using namespace std;
int alternating_sum(vector<int>& v);

int main()
{
    vector<int> alterSum;
    int nums;
    cout << "Enter real numbers and get the alternating sum. (non-number to quit): ";
    while (cin >> nums)
    {
        alterSum.push_back(nums);
    }
    cout << "The alternating sum of your inputs is " << alternating_sum(alterSum) << endl;
    return 0;
}
int alternating_sum(vector<int>& v)
{
    int alterSum = 0;
    for (int i = 0;i < v.size(); i++)
    {
        if ((i % 2) == 0)
        	alterSum = alterSum + v[i]; 
        else
        	alterSum = alterSum - v[i]; 
    }
    return alterSum;
}